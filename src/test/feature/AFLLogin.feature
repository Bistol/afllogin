Feature: To verify AFL Login
  As a user
  I want to login to AFL
  So that I can see my profile

Scenario Outline: Valid AFL login test

  Given I am using browser "<browser>"
  And I am on the login page
  When I enter "<username>" and "<password>"
  And click on login button
  Then i am logged in

  Examples:
  |browser|username|password|
  |chrome|fptest@mailinator.com|Tester123|
