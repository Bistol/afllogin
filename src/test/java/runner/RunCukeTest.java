package runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import gherkin.formatter.model.Feature;
import org.junit.runner.RunWith;

/**
 * Created by Bistol on 24/04/2016.
 */

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "./src/test/feature",
        glue = "definition",
        plugin = {"pretty",
                "html:target/html",
                "json:target/mss-registration.json"
        },
        tags = "~@wip"

)
public class RunCukeTest {

}
