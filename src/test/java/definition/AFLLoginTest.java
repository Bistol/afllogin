package definition;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


import java.util.concurrent.TimeUnit;

/**
 * Created by Bistol on 24/04/2016.
 */
public class AFLLoginTest {

    private WebDriver driver;
    private Scenario sce;
    private String baseUrl;
    private AFLLoginPage aflLoginPage;
    private AFLHomePage aflHomePage;

    @Before
    public void testInitialise(){
        baseUrl = "https://signon.telstra.com/login?goto=https%3A%2F%2Fsignon.telstra.com%2Ffederation%2Fsaml2%3FSPID%3DAFLMedia%26furl%3Dhttp%253A%252F%252Fwww.afl.com.au%252F%26curl%3Dhttp%253A%252F%252Fwww.afl.com.au%252F&gotoNoTok=";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Given("^I am using browser \"([^\"]*)\"$")
    public void i_am_using_browser(String arg1) throws Throwable {
        driver = new FirefoxDriver();

    }

    @Given("^I am on the login page$")
    public void i_am_on_the_login_page() throws Throwable {
        driver.get(baseUrl);

    }

    @When("^I enter \"([^\"]*)\" and \"([^\"]*)\"$")
    public void i_enter_and(String arg1, String arg2) throws Throwable {


    }

    @When("^click on login button$")
    public void click_on_login_button() throws Throwable {

    }

    @Then("^i am logged in$")
    public void i_am_logged_in() throws Throwable {

    }

    @After
    public void tearDown(){
        if (driver != null) {
            driver.quit();
        }
    }

}
